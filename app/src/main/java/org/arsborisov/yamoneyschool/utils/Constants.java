package org.arsborisov.yamoneyschool.utils;

public interface Constants {
    interface TAG {
        String FRAGMENT_LIST = "fragment_list";
    }

    interface URL {
        String API = "https://money.yandex.ru/api/categories-list";
    }

    interface DB {
        String DB_NAME = "mydb";
        int DB_VERSION = 1;
        String ITEM_TABLE = "items";
    }
}
