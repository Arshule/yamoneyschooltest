package org.arsborisov.yamoneyschool.api;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class ItemDTO implements Parcelable {
    public static final Parcelable.Creator<ItemDTO> CREATOR = new Parcelable.Creator<ItemDTO>() {
        public ItemDTO createFromParcel(Parcel source) {
            return new ItemDTO(source);
        }

        public ItemDTO[] newArray(int size) {
            return new ItemDTO[size];
        }
    };
    private int id;
    private String title;
    private List<ItemDTO> subs;

    protected ItemDTO(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.subs = new ArrayList<>();
        in.readList(this.subs, List.class.getClassLoader());
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<ItemDTO> getSubs() {
        return subs;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeList(this.subs);
    }
}
