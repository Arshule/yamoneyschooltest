package org.arsborisov.yamoneyschool;

import android.app.Application;

import com.raizlabs.android.dbflow.config.FlowManager;

public class AppDelegate extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FlowManager.init(this);
    }
}
