package org.arsborisov.yamoneyschool.database;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import org.arsborisov.yamoneyschool.utils.Constants;

@Table(databaseName = Constants.DB.DB_NAME, tableName = Constants.DB.ITEM_TABLE)
public class MyItem extends BaseModel {

    @Column
    @PrimaryKey(autoincrement = true)
    long _id;

    @Column
    long item_id;

    @Column
    String title;

    @Column
    long parent_id;

    @Column
    boolean hasChild;

    public MyItem() {
    }

    public MyItem(long item_id, String title, long parent_id) {
        this.item_id = item_id;
        this.title = title;
        this.parent_id = parent_id;
    }

    public long get_id() {
        return _id;
    }

    public String getTitle() {
        return title;
    }

    public boolean isHasChild() {
        return hasChild;
    }

    public void setHasChild(boolean hasChild) {
        this.hasChild = hasChild;
    }
}
