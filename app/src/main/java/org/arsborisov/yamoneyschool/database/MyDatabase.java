package org.arsborisov.yamoneyschool.database;

import com.raizlabs.android.dbflow.annotation.Database;

import org.arsborisov.yamoneyschool.utils.Constants;

@SuppressWarnings("unused")
@Database(name = Constants.DB.DB_NAME, version = Constants.DB.DB_VERSION)
public class MyDatabase {

}
