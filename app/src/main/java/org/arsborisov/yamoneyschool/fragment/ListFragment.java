package org.arsborisov.yamoneyschool.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.raizlabs.android.dbflow.sql.builder.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;

import org.arsborisov.yamoneyschool.R;
import org.arsborisov.yamoneyschool.adapter.ListAdapter;
import org.arsborisov.yamoneyschool.database.MyItem;
import org.arsborisov.yamoneyschool.database.MyItem$Table;

import java.util.List;

public class ListFragment extends android.support.v4.app.Fragment {

    private static final String PARENT_ID_BUNDLE = "parentId";

    private OnListFragmentCallback onListFragmentCallback;
    private ListAdapter mAdapter;

    public static ListFragment newInstance(long parentId) {
        Bundle args = new Bundle();
        args.putLong(PARENT_ID_BUNDLE, parentId);
        ListFragment fragment = new ListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onListFragmentCallback = (OnListFragmentCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnListFragmentCallback");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        long parentId = getArguments().getLong(PARENT_ID_BUNDLE);

        List<MyItem> myItems = new Select().from(MyItem.class)
                .where(Condition.column(MyItem$Table.PARENT_ID)
                        .eq(parentId)).queryList();
        mAdapter = new ListAdapter(getActivity(), myItems);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list, container, false);
        ListView listView = (ListView) v.findViewById(R.id.fragment_list_lv);
        listView.setAdapter(mAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onListFragmentCallback.onItemClick(mAdapter.getItem(position));
            }
        });

        SwipeRefreshLayout swipeContainer = (SwipeRefreshLayout) v.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onListFragmentCallback.onRefresh();
            }
        });

        swipeContainer.setColorSchemeResources(R.color.blue_bright,
                R.color.green_light,
                R.color.orange_light,
                R.color.red_light);

        return v;
    }

    public interface OnListFragmentCallback {
        void onItemClick(MyItem item);

        void onRefresh();
    }
}