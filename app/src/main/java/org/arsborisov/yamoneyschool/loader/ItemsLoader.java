package org.arsborisov.yamoneyschool.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.arsborisov.yamoneyschool.api.ItemDTO;
import org.arsborisov.yamoneyschool.database.MyItem;
import org.arsborisov.yamoneyschool.utils.Constants;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

public class ItemsLoader extends AsyncTaskLoader<List<ItemDTO>> {

    public static final long ROOT_PARENT_ID = 0;

    public ItemsLoader(Context context) {
        super(context);
    }

    @Override
    public List<ItemDTO> loadInBackground() {
        List<ItemDTO> itemDTOList = null;
        try {
            JSONArray upgradeQuery = getListQuery();

            Type listType = new TypeToken<List<ItemDTO>>() {
            }.getType();
            itemDTOList = new Gson().fromJson(upgradeQuery.toString(), listType);

            if (itemDTOList != null) {
                Delete.tables(MyItem.class);
                for (ItemDTO item : itemDTOList) {
                    insertItem(item, ROOT_PARENT_ID);
                }
            }
        } catch (IOException | JSONException e) {
            Log.e(getClass().getName(), "Unable to connect");
            e.printStackTrace();
        }
        return itemDTOList;
    }

    private void insertItem(ItemDTO itemDTO, long parentId) {
        MyItem item = new MyItem(itemDTO.getId(), itemDTO.getTitle(), parentId);

        if (itemDTO.getSubs() != null && !itemDTO.getSubs().isEmpty()) {
            item.setHasChild(true);
            item.insert();

            for (ItemDTO dto : itemDTO.getSubs()) {
                insertItem(dto, item.get_id());
            }
        } else {
            item.insert();
        }
    }

    private JSONArray getListQuery() throws IOException, JSONException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(Constants.URL.API)
                .build();
        Response response = client.newCall(request).execute();
        return new JSONArray(response.body().string());
    }
}