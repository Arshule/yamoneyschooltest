package org.arsborisov.yamoneyschool.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.arsborisov.yamoneyschool.R;
import org.arsborisov.yamoneyschool.database.MyItem;

import java.util.List;

public class ListAdapter extends BaseAdapter {

    private List<MyItem> mItemList;
    private Context mContext;

    public ListAdapter(Context context, List<MyItem> itemList) {
        mContext = context;
        mItemList = itemList;
    }

    @Override
    public int getCount() {
        return mItemList.size();
    }

    @Override
    public MyItem getItem(int position) {
        return mItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.list_item_category_title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.title.setText(mItemList.get(position).getTitle());
        if (mItemList.get(position).isHasChild()) {
            viewHolder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_right, 0);
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView title;
    }
}
