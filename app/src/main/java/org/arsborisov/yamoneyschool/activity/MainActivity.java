package org.arsborisov.yamoneyschool.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import org.arsborisov.yamoneyschool.R;
import org.arsborisov.yamoneyschool.api.ItemDTO;
import org.arsborisov.yamoneyschool.database.MyItem;
import org.arsborisov.yamoneyschool.fragment.ListFragment;
import org.arsborisov.yamoneyschool.loader.ItemsLoader;
import org.arsborisov.yamoneyschool.utils.Constants;

import java.util.List;

public class MainActivity extends AppCompatActivity implements ListFragment.OnListFragmentCallback, LoaderManager.LoaderCallbacks<List<ItemDTO>> {

    public static final long ROOT_PARENT_ID = 0;
    public static final String IS_ITEM_IN_DB_PREFS = "is_item_in_db";
    public static final int ITEMS_LOADER_ID = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.mipmap.ic_launcher);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        final Loader<List<ItemDTO>> itemLoader = getSupportLoaderManager().initLoader(ITEMS_LOADER_ID, Bundle.EMPTY, this);
        if (savedInstanceState == null) {
            if (getPreferences(MODE_PRIVATE).getBoolean(IS_ITEM_IN_DB_PREFS, false)) {
                showFragment(R.id.activity_main_container, ListFragment.newInstance(ROOT_PARENT_ID),
                        Constants.TAG.FRAGMENT_LIST, false);
            } else {
                itemLoader.forceLoad();
            }
        }
    }

    private void showFragment(int layout, Fragment fragment, String tag, boolean needAddToBackStack) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit,
                R.anim.fragment_pop_enter, R.anim.fragment_pop_exit);
        ft.replace(layout, fragment, tag);
        if (needAddToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }

    @Override
    public void onItemClick(MyItem item) {
        if (item.isHasChild()) {
            showFragment(R.id.activity_main_container, ListFragment.newInstance(item.get_id()),
                    Constants.TAG.FRAGMENT_LIST, true);
        }
    }

    @Override
    public void onRefresh() {
        getSupportLoaderManager().initLoader(ITEMS_LOADER_ID, Bundle.EMPTY, this).forceLoad();
    }

    @Override
    public Loader<List<ItemDTO>> onCreateLoader(int id, Bundle args) {
        Loader<List<ItemDTO>> loader = null;
        if (id == ITEMS_LOADER_ID) {
            loader = new ItemsLoader(this);
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<ItemDTO>> loader, List<ItemDTO> data) {
        Log.d("loader", "finished");
        if (findViewById(R.id.swipeContainer) != null) {
            ((SwipeRefreshLayout) findViewById(R.id.swipeContainer)).setRefreshing(false);
        }
        if (data == null) {
            Toast.makeText(MainActivity.this, R.string.unable_to_connect, Toast.LENGTH_LONG).show();
            return;
        }

        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
        editor.putBoolean(IS_ITEM_IN_DB_PREFS, true);
        editor.commit();

        finish();
        startActivity(getIntent());
    }

    @Override
    public void onLoaderReset(Loader<List<ItemDTO>> loader) {
        Log.d("loader", "reset");
    }
}